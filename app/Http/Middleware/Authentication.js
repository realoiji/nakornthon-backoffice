'use strict'

class Authentication {

  * checkLogin(request, response) {
    const dataResponse = {};
    const errors = [];

    const isLoggedIn = yield request.auth.check();
    if (!isLoggedIn) {
      errors.push({
        code: 401,
        message: 'unauthorized',
      });
    }
    if (errors.length > 0) {
      dataResponse.errors = errors;
    } else {
      dataResponse.data = 'ok';
    }

    return dataResponse;
    // response.json(dataResponse);
  }

  * handle (request, response, next) {
    const dataResponse = yield this.checkLogin(request, response);
    if (dataResponse.errors) {
      response.json(dataResponse);
    }

    yield next
  }

}

module.exports = Authentication
