'use strict'

const Database = use('Database');
const Hash = use('Hash');
const Validator = use('Validator')
const User = use('App/Model/User');
const Token = use('App/Model/Token');

class UserController {

  * index (request, response) {
    const users = yield User.all();
    response.json(users);
  }

  * getToken (request) {
    return request.headers().authorization.replace('Bearer ', '');
  }

  * findUserFromToken (tokenValue) {
    const token = yield Token.findBy('token', tokenValue);
    yield token.related('user').load();
    return token;
  }

  * login(request, response) {
    const dataResponse = {};
    const errors = [];

    const data = request.only('username', 'password');
    const validation = yield Validator.validateAll(data, {
      username: 'required',
      password: 'required',
    });
    if (validation.fails()) {
      validation.messages().map((val, index) => {
        errors.push({
          code: 400,
          message: val.message,
        });
        return
      });
    }
    const user = yield User.query().where('username', data.username).first();
    if (user) {
      const isSame = yield Hash.verify(data.password, user.password);
      if (isSame) {
        yield request.auth.revokeAll(user);
        const token = yield request.auth.generate(user);
        if (token) {
          dataResponse.data = {
            user: user,
            token: token,
          };
        } else {
          errors.push({
            code: 502,
            message: 'badGateway',
          });
        }
      } else {
        errors.push({
          code: 401,
          message: 'Password wrong',
        });
      }
    } else {
      errors.push({
        code: 401,
        message: 'Username missing',
      });
    }

    if (errors.length > 0) {
      dataResponse.errors = errors;
    }
    response.json(dataResponse);
  }

  * checkUserFromToken(request, response) {
    const dataResponse = {};
    const errors = [];

    const tokenValue = yield this.getToken(request);
    const token = yield this.findUserFromToken(tokenValue);
    if (token) {
      dataResponse.data = token;
    } else {
      errors.push({
        code: 401,
        message: 'token missing',
      });
    }
    if (errors.length > 0) {
      dataResponse.errors = errors;
    }
    response.json(dataResponse);
  }

  * logout(request, response) {
    const dataResponse = {};
    const errors = [];

    const tokenValue = yield this.getToken(request);
    const token = yield this.findUserFromToken(tokenValue);
    // console.log('token', token.toJSON().user.id);

    if (token) {
      const id = token.toJSON().user.id;
      if (id) {
        const user = yield User.find(id);
        // response.json(user);
        const revoke = yield request.auth.revokeAll(user);
        if (revoke) {
          dataResponse.data = 'ok';
        } else {
          errors.push({
            code: 401,
            message: 'User is never authentication',
          });
        }
      } else {
        errors.push({
          code: 401,
          message: 'user id missing',
        });
      }
    } else {
      errors.push({
        code: 401,
        message: 'token missing',
      });
    }
    if (errors.length > 0) {
      dataResponse.errors = errors;
    }

    response.json(dataResponse);
  }

  * create(request, response) {
    //
  }

  * store(request, response) {
    //
  }

  * show(request, response) {
    //
  }

  * edit(request, response) {
    //
  }

  * update(request, response) {
    //
  }

  * destroy(request, response) {
    //
  }

}

module.exports = UserController
