'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.on('/').render('welcome')

Route.post('/login', 'UserController.login');

Route.group('user', function () {
  Route.get('/', 'UserController.index');
  Route.get('checkUserFromToken', 'UserController.checkUserFromToken');
  Route.get('logout', 'UserController.logout');
}).prefix('api/user').middleware('authen');
